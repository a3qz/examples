// duplicates_linear.cpp

#include <algorithm>
#include <climits>
#include <iostream>
#include <random>
#include <vector>

using namespace std;

const int N = 1<<15;

int main(int argc, char *argv[]) {
    random_device		rd;
    default_random_engine	g(rd());
    uniform_int_distribution<>	d(1, INT_MAX);
    vector<int>			v;

    for (int i = 0; i < N; i++) {
    	v.push_back(d(g));
    }

    for (auto it = v.begin(); it != v.end(); it++) {
    	if (find(it + 1, v.end(), *it) != v.end()) {
    	    cout << *it << " is duplicated" << endl;
    	    break;
	}
    }

    return 0;
}
